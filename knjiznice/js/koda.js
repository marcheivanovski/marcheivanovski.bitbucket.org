
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var Oseba1={
   ime: "Tilen",
   primek: "Slavec",
   adresa: "Tržaška 5",
   datum: "1945-10-02",
   tezina: 80,
   visina: 170,
   temperatura: 37,
   skt: 160,
   dkt: 120,
   srcniutrip: 55,
   problem: "high blood pressure",
   tezine: [96,95, 93, 89, 85, 80],
   temperature: [37.6, 38, 37.5, 36.9, 38.6, 37],
   srcniutripe: [65 ,60, 58, 80, 51, 55],
   visine: [185, 179, 176, 174, 171, 167],
   skte: [150, 168, 180, 173, 165, 170],
   dkte: [95, 100, 105, 92, 115, 120]
 };
 var Oseba2={
   ime: "Janko",
   primek: "Babnik",
   adresa: "Mencingerjeva 30",
   datum: "1999-08-05",
   tezina: 95,
   visina: 195,
   temperatura: 36.6,
   skt: 120,
   dkt: 90,
   srcniutrip: 80,
   problem: "selfcare", //ta oseba nima problem zato dobi podatki o sebi negi
   tezine: [75, 78, 84, 86, 89, 95],
   temperature: [36.7, 36.5, 36.9, 36.8, 37, 36.8],
   srcniutripe: [74, 77, 75, 73, 78, 80],
   visine: [170, 177, 180, 183, 189, 195],
   skte: [135, 138, 130, 133, 145, 120],
   dkte: [75, 85, 77, 90, 87, 90]
 };
 var Oseba3={
   ime: "Joze",
   primek: "Babic",
   adresa: "Askerceva 20",
   datum: "1978-03-06",
   tezina: 45,
   visina: 160,
   temperatura: 35.9,
   skt: 140,
   dkt: 78,
   srcniutrip: 80,
   problem: "underweight",
   tezine: [31, 34, 36, 39, 36, 40],
   temperature: [36.5, 36, 38.5, 37.1, 36.6, 35.9],
   srcniutripe: [78 ,80, 95, 91, 89, 80],
   visine: [148, 149, 150, 151, 154, 160],
   skte: [150, 158, 140, 143, 155, 140],
   dkte: [95, 85, 79, 72, 80, 78]
 };
 
function generirajOseba(){
  for(var i=1; i<=3; i++){
    generirajPodatke(i);
  }
}
 
function generirajPodatke(stPacienta) {
  //ehrId = "";
  //console.log(stPacienta);
  // TODO: Potrebno implementirati
  var oseba;
  if(stPacienta==1){
    oseba=Oseba1;
  }else if(stPacienta==2){
    oseba=Oseba2;
  }else if(stPacienta==3){
    oseba=Oseba3;
  }
  
  var polje= $("#preberiObstojeciEHR").val();
  //console.log(polje);
  if(polje==null){
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        oseba.ehrId=data.ehrId;
        console.log(oseba.ehrId+" "+oseba.ime);
        var partyData = {
          firstNames: oseba.ime,
          lastNames: oseba.primek,
          dateOfBirth: oseba.datum,
          additionalInfo: {"ehrId": ehrId, "adresa": oseba.adresa , "tezina": oseba.tezina , "visina": oseba.visina, "temperatura": oseba.temperatura, "skt": oseba.skt, "dkt": oseba.dkt, "srcniutrip": oseba.srcniutrip, "problem": oseba.problem  }
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              $("#preberiObstojeciEHR").append("<option value="+stPacienta +">"+oseba.ime + " "+oseba.primek+ "</option>");
              alert("Uspesno je bila kreirana oseba "+ oseba.ime + " "+ oseba.primek + " z ehr-id " + ehrId );
            }
          },
          error: function(err) {
          	console.log("NAPAKA");
          }
        });
      }
		});
  }
  //return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function preberiPoIme(){
  var ime=$("#preberiObstojeciEHR").val();
  //console.log(ime);
  var ehrId;
  if(ime==1){
    ehrId=Oseba1.ehrId;
  }else if(ime==2){
    ehrId=Oseba2.ehrId;
  }else if(ime==3){
    ehrId=Oseba3.ehrId;
  }
  $("#preberiEHR").val(ehrId);
  
  	$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			//console.log(data);
  			$("#Ime").val(data.party.firstNames);
  			$("#Priimek").val(data.party.lastNames);
  			$("#Naslov").val(data.party.additionalInfo.adresa);
  			$("#EHR").val(ehrId);
  			$("#DatumRojstva").val(data.party.dateOfBirth);
  			$("#Tezina").val(data.party.additionalInfo.tezina);
  			$("#Visina").val(data.party.additionalInfo.visina);
  			$("#Temperatura").val(data.party.additionalInfo.temperatura);
  			$("#SKT").val(data.party.additionalInfo.skt);
  			$("#DKT").val(data.party.additionalInfo.dkt);
  			$("#SrcniUtrip").val(data.party.additionalInfo.srcniutrip);
  			document.getElementById("preberiNegativnoSporocilo").innerHTML="";
  		  document.getElementById("preberiSporocilo").innerHTML="Uspešno ste izbrali osebo "+data.party.firstNames+" "+data.party.lastNames;

  		  ustvariGraf(data.party.firstNames);
  		  $.ajax({
	            	url: 'https://api.duckduckgo.com/?q='+data.party.additionalInfo.problem+ '&format=json&pretty=1',
	             	type: 'GET',
	            	dataType: 'jsonp',
	                	
	             	success: function(results){
	              		//console.log(results);
	              		var WIKI="See more on Wikipedia.";
	              		$("#problemResitev").html("");
	              		$("#problemResitev").append($('<a href="'+results.AbstractURL+'">'+WIKI+'</a>'));
	              		$("#problemResitev").append(results.Abstract);
			 	           	for(var i=0; i<results.RelatedTopics.length; i++){
	               			$("#problemResitev").append("<p>"+results.RelatedTopics[i].Result+"</p>");
			 	           	}
	             	}
	      });
	      if(myChart!=null){
          myChart.destroy();  //ce graf ze obstaja
        }
  		},
  		error: function(err) {
  			console.log("NAPAKA");
  		  document.getElementById("preberiSporocilo").innerHTML="";
        document.getElementById("preberiNegativnoSporocilo").innerHTML="Prišlo je do napake";
  		}
		});
}

function preberiPoEHR(){
    var ehrId=$("#preberiEHR").val();
    //console.log(ehrId);'
    var obstaja=0;
    
    if( ehrId.localeCompare(Oseba1.ehrId)==0 ){
      $("#preberiObstojeciEHR").val("");
      obstaja=1;
    }
    if(ehrId.localeCompare(Oseba2.ehrId)==0){
      $("#preberiObstojeciEHR").val("");
      obstaja=1;
    }
    if(ehrId.localeCompare(Oseba3.ehrId)==0){
      $("#preberiObstojeciEHR").val("");
      obstaja=1;
    }
    if(obstaja==1){
      $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
  			console.log(data);
  			$("#Ime").val(data.party.firstNames);
  			$("#Priimek").val(data.party.lastNames);
  			$("#Naslov").val(data.party.additionalInfo.adresa);
  			$("#EHR").val(ehrId);
  			$("#DatumRojstva").val(data.party.dateOfBirth);
  			$("#Tezina").val(data.party.additionalInfo.tezina);
  			$("#Visina").val(data.party.additionalInfo.visina);
  			$("#Temperatura").val(data.party.additionalInfo.temperatura);
  			$("#SKT").val(data.party.additionalInfo.skt);
  			$("#DKT").val(data.party.additionalInfo.dkt);
  			$("#SrcniUtrip").val(data.party.additionalInfo.srcniutrip);
  			document.getElementById("preberiNegativnoSporocilo").innerHTML="";
  			document.getElementById("preberiSporocilo").innerHTML="Uspesno ste izbrali osebo "+data.party.firstNames+" "+data.party.lastNames;
  			
  		  ustvariGraf(data.party.firstNames);
  		  $.ajax({
	            	url: 'https://api.duckduckgo.com/?q='+data.party.additionalInfo.problem+ '&format=json&pretty=1',
	             	type: 'GET',
	            	dataType: 'jsonp',
	                	
	             	success: function(results){
	              		//console.log(results);
	              		var WIKI="See more on Wikipedia.";
	              		$("#problemResitev").html("");
	              		$("#problemResitev").append($('<a href="'+results.AbstractURL+'">'+WIKI+'</a>'));
	              		$("#problemResitev").append(results.Abstract);
			 	           	for(var i=0; i<results.RelatedTopics.length; i++){
	               			$("#problemResitev").append("<p>"+results.RelatedTopics[i].Result+"</p>");
			 	           	}
	             	}
	      });
	      if(myChart!=null){
           myChart.destroy();  //ce graf ze obstaja
        }
  		},
  		error: function(err) {
  			console.log("NAPAKA");
  		}
		});
    }
    else{
      document.getElementById("preberiSporocilo").innerHTML="";
      document.getElementById("preberiNegativnoSporocilo").innerHTML="Zal ta EHR-id ne obstaja";
    }
    
    
    
}
function ustvariGraf(name){
  var person;
  $("#chartContainer").html("");
  console.log(name);
  if(name.localeCompare("Tilen")==0){
    person=Oseba1;
  }
  else if(name.localeCompare("Janko")==0){
    person=Oseba2;
  }
  else if(name.localeCompare("Joze")==0){
    person=Oseba3;
  }
  var chart = new CanvasJS.Chart("chartContainer",{
      animationEnabled: true,
  		title:{
  			text: "Klik na stolpec za prikaz podrobnosti"
  		},
  		axisX:{
  		interval: 1
  	  },
  		data: [
  		{
  			type: "column",
  			click: onClick,
  			dataPoints: [
  					{ y: person.tezina, label: "Teža" },
      			{ y: person.visina, label: "Visina" },
      			{ y: person.temperatura, label: "Temperatura" },
      			{ y: person.skt, label: "Sistolični krvni tlak" },
      			{ y: person.dkt, label: "Diastolični krvni tlak" },
      			{ y: person.srcniutrip, label: "Srčni Utrip"}
  			]
  		}]
  	});
  	chart.render();
  
    function onClick(e){
      console.log(e);
      var data;
      if(e.dataPointIndex==0){  //tezina
        data=person.tezine;
      }else if(e.dataPointIndex==1){
        data=person.visine;
      }else if(e.dataPointIndex==2){
        data=person.temperature;
      }else if(e.dataPointIndex==3){
        data=person.skte;
      }else if(e.dataPointIndex==4){
        data=person.dkte;
      }else if(e.dataPointIndex==5){
        data=person.srcniutripe;
      }
      grafPodrobnosti(e.dataPoint.label,data );
    }
}

var myChart;
function grafPodrobnosti(title,data){
  //console.log(myChart);
  if(myChart!=null){
    myChart.destroy();  //ce graf ze obstaja
  }
  var ctx= document.getElementById('myChart').getContext('2d');
  myChart = new Chart(ctx, {
      type: 'line',
      data: {
          labels: ['2014', '2015', '2016', '2017', '2018', '2019'],
          datasets: [{
              label: title,
              data: data,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
}